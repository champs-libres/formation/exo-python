# Python exercices 

## 01 spread_the_word.py

The code is not optimal at all. Rewrite the code of the function `spread_the_word` using adapted data structures.

The computation time can pass for `6` secs to `0.06` sec for a population of `25000` persons.