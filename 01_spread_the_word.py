import random
from datetime import datetime

def ask_to_spead(person_id, population_number):
    """
    Contacts a person and ask her to contact another 5 persons

    Parameters
    ----------
    person_id : int
        the id of the person contacted
    population_number: int
        the number of persons of the population


    Returns
    -------
    list of int
        The list of person id that the contacted person will contact.
    """    
    
    persons_to_contact = [random.randint(0, population_number) for _ in range(5)]
    return persons_to_contact


def spread_the_word(population_number):
    """
    A message has to been given to each person of a population.
    The chosed method is to contact some persons, inform them and
    ask them to also contact 5 other persons to inform them.

    Parameters
    ----------
    population_number: int
        the size of the population
    """

    persons_to_contact = list(range(population_number))

    while len(persons_to_contact) > 0:
        # chose one person in the population
        person_id = persons_to_contact[0]

        # contact them and ask them to contact 5 other persons
        contacted_persons = ask_to_spead(person_id, population_number)
        
        contacted_persons.append(person_id)

        # update the list of persons to contact
        persons_to_contact = [p for p in persons_to_contact if p not in contacted_persons]


if __name__ == '__main__':
    start = datetime.now()
    spread_the_word(25000)
    computation_time = datetime.now() - start
    print(f"Computation time : {computation_time}")